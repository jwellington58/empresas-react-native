import 'react-native-gesture-handler';
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useLayoutEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import AsyncStorage from '@react-native-community/async-storage';
import api from './services/ApiService';
import { Login, Home, Enterprise } from './views';

const Stack = createStackNavigator();

const App: () => React$Node = () => {
  const [login, setLogin] = useState(null);
  useLayoutEffect(() => {
    AsyncStorage.getItem('access-token').then((token) => {
      AsyncStorage.getItem('client').then((client) => {
        AsyncStorage.getItem('uid').then((uid) => {
          AsyncStorage.getItem('expiry')
            .then((expiry) => {
              const dateExpiry = new Date(expiry * 1000);
              if (dateExpiry.getTime() > new Date().getTime()) {
                api.defaults.headers = {
                  ...api.defaults.headers,
                  'access-token': token,
                  uid,
                  client,
                };
                setLogin(true);
              } else {
                setLogin(false);
              }
            })
            .catch(() => {
              setLogin(false);
            });
        });
      });
    });
  }, []);
  return (
    <NavigationContainer>
      <>
        {login !== null ? (
          <>
            {login ? (
              <Stack.Navigator
                screenOptions={{
                  headerShown: false,
                }}
              >
                <Stack.Screen name="Home" component={Home} />
                <Stack.Screen name="Enterprise" component={Enterprise} />
                <Stack.Screen name="Login" component={Login} />
              </Stack.Navigator>
            ) : (
              <Stack.Navigator
                screenOptions={{
                  headerShown: false,
                }}
              >
                <Stack.Screen name="Login" component={Login} />
                <Stack.Screen name="Home" component={Home} />
                <Stack.Screen name="Enterprise" component={Enterprise} />
              </Stack.Navigator>
            )}
          </>
        ) : null}
      </>
    </NavigationContainer>
  );
};

export default App;
