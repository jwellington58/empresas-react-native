import axios from 'axios';

const baseUrl = 'https://empresas.ioasys.com.br/api/v1/';
const api = axios.create({ baseURL: baseUrl });

export default api;
