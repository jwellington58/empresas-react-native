![N|Solid](logo_ioasys.png)

# Desafio React Native - ioasys

Projeto de listagem de empresas, usando react native.

---
## Descrição
  Conforme solicitado no teste, foi implementado uma aplicação simples para consumir os endpoints fornecidos para o teste.
 O projeto é composto por basicamente 3 telas. Uma tela de 'Login', onde é feito o processo de autenticação do usuário 
 através de uma requisição POST no endpoint:  https://empresas.ioasys.com.br/api/v1/users/auth/sign_in. Em caso de 
 sucesso, os headers retornados (access_token, uid, client)são salvos em AsyncStorage e são adicionados como headers padrão 
 do axios (biblioteca cliente HTTP escolhida para fazer requisições no projeto), e ocorre a navegação para tela de Home.
 Na tela de Home, basicamente são consumidos dois endpoints. Primeiro, é feito uma requisição em https://empresas.ioasys.com.br/api/v1/enterprises,
 para listar todas as empresas cadastradas na base. Com o retorno da API é renderizado um ScrollView com uma lista de cards,
 onde cada card é uma empresa com suas devidas informações (nome, local e foto). Na listagem de empresas, não exibi as fotos
 retornadas pela API, pois não consegui acessar as mesmas, por isso, deixei a foto estática. O outro endpoint consumido na tela de Home
 é o de filtro (https://empresas.ioasys.com.br/api/v1/enterprises?name), onde através de um componente de busca eu realizei a chamada 
 na API e exibir apenas as empresas retornadas no filtro. Vale ressaltar, que além do nome da empresa, este endpoint
 fornecia a opção de filtro pelo tipo da empresa, contudo, como eu não tinha conhecimento sobre quais eram os possíveis tipos,
 optei por usar apenas o nome. Por fim, criei a tela de Enterprise. Essa tela exibi os detalhes de uma empresa, como redes sociais,
 contato , etc. Para acionar essa tela, basta clicar em um dos cards da tela de Home, dessa forma, o Id da empresa que foi clicado
 é passado via rota e o endpoint ( https://empresas.ioasys.com.br/api/v1/enterprises/id) é acionado.
 
## Estrutura
 - assets: Para armazenar os recursos do proejto. Ex: Imagens.
 - components: Pasta onde foram colocados os componentes reutilizáveis da aplicação. Ex: Botões e Cards
 - services: Onde axios foi configurado, através dele foram feitas as requisiçãoes HTTPs. 
 - theme: Onde foram configuradas algumas constante relacionas ao estilo do app. Ex: tamanhos de fonte padrão e cores.
 - views: Onde foram criadas as telas do sistema. Ex: Login e Home.

## Bibliotecas
- @react-native-community/async-storage: Para armazenar as informações como access_token localmente.
- react-navigation: Para prover a navegação entre telas e sistema de rotas do app.
- axios: Cliente HTTP usado para fazer requisições na API disponibilizada.
- debounce: Usado para dar o intervalo na busca, para evitar que seja feita uma requisição na API a cada letra digitada.
- react-native-vector-icons: Biblioteca de ícones usada na aplicação desenvolvida.

## Configuração de Ambiente e Execução do Projeto
 Para configurar o ambiente no windows ou ubuntu, recomendo este tutorial: https://medium.com/@pedroaugusto466/configurando-ambiente-de-desenvolvimento-para-react-native-4049709654ed
 
 Para rodar o projetoa, navegue até a pasta raiz do projeto (Empresas)
 
  - Execute o comando **yarn** para instalar as dependencias do projeto.
  - Execute o comando **react-native run-android** para rodar o projeto no android.
	
